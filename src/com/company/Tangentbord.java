package com.company;


public class Tangentbord {

    String color;

    public Tangentbord(Main.colors color) {

        this.color = colorVerify(color);
    }

    private String colorVerify(Main.colors color) {
        switch (color) {
            case BLACK -> {
                return "BLACK";
            }
            case GRAY -> {
                return "GRAY";
            }
            case WHITE -> {
                return "WHITE";
            }
            default -> {
                return "";
            }

        }
    }
}
