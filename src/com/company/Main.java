package com.company;

import java.sql.SQLOutput;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    enum colors{
        BLACK, WHITE, GRAY
    }


    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Skriv din färg");


        colors value = null;
        while (value == null) {
            try {
                value = colors.valueOf(scan.nextLine().toUpperCase());
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                System.out.println("Skriv in en ny färg");
            }
        }



        Tangentbord tbord1 = new Tangentbord(value);
        displayColor(tbord1.color);


        scan.close();
    }

    public static void displayColor(String str){
        System.out.printf("Din färg är %s%s", str.charAt(0), str.substring(1).toLowerCase());

    }


}



